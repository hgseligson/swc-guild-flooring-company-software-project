﻿using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace FlooringProgram.Tests
{
    [TestFixture]
    public class BLLTest
    {
        [TearDown]
        public void Finish()
        {
            string filePath = Environment.CurrentDirectory;
            string realFilePath = Path.GetFullPath(Path.Combine(filePath, @"..\..\"));
            var files = Directory.GetFiles(realFilePath, "Orders_*.txt");
            foreach (var file in files)
            {
                File.Delete(file);
            }
        }

        [Test()]
        public void ShouldReturnResponseWithSuccessCreateOrder()
        {
            //arrange
            var target = new OrderOperations();
            var expected = 1;
            var order = new Order
            {
                TaxInfo = new TaxInfo
                {
                    TaxRate = .1m,
                    StateAbbreviation = "OH"
                },
                ProdInfo = new ProductInfo
                {
                    ProductType = "Wood",
                    CostPerSquareFoot = 10,
                    LaborPerSquareFoot = 1
                },
                CustomerName = "Bob",
                Area = 10
            };
            //act
            var actual = target.AddOrder(DateTime.Now.ToString("MMddyyyyy"), order);
            //assert
            Assert.AreEqual(expected, actual.Order.OrderNumber);
        }

        [Test()]
        public void ShouldReturnResponseWithSuccessGetOrderById()
        {
            //arrange
            var target = new OrderOperations();
            var date = DateTime.Now.ToString("MMddyyyyy");
            var order = new Order
            {
                TaxInfo = new TaxInfo
                {
                    TaxRate = .1m,
                    StateAbbreviation = "OH"
                },
                ProdInfo = new ProductInfo
                {
                    ProductType = "Wood",
                    CostPerSquareFoot = 10,
                    LaborPerSquareFoot = 1
                },
                CustomerName = "Bob",
                Area = 10
            };
            //act
            Order expected = target.AddOrder(date, order).Order;
            Order actual = target.GetOrder(date, expected.OrderNumber).Order;
            //assert
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void ShouldReturnResponseWithSuccessGetOrderList()
        {
            //arrange
            var target = new OrderOperations();
            var date = DateTime.Now.ToString("MMddyyyyy");
            var order = new Order
            {
                TaxInfo = new TaxInfo
                {
                    TaxRate = .1m,
                    StateAbbreviation = "OH"
                },
                ProdInfo = new ProductInfo
                {
                    ProductType = "Wood",
                    CostPerSquareFoot = 10,
                    LaborPerSquareFoot = 1
                },
                CustomerName = "Bob",
                Area = 10
            };
            //act
            Order expected = target.AddOrder(date, order).Order;
            List<Order> actual = target.GetOrderList(date).OrdersForDate;
            //assert
            Assert.AreEqual(expected, actual[0]);
        }

        [Test()]
        public void ShouldReturnResponseWithRemoveOrder()
        {
            //arrange
            var target = new OrderOperations();
            var date = DateTime.Now.ToString("MMddyyyyy");
            var order = new Order
            {
                TaxInfo = new TaxInfo
                {
                    TaxRate = .1m,
                    StateAbbreviation = "OH"
                },
                ProdInfo = new ProductInfo
                {
                    ProductType = "Wood",
                    CostPerSquareFoot = 10,
                    LaborPerSquareFoot = 1
                },
                CustomerName = "Bob",
                Area = 10
            };
            //act
            Order actual = target.AddOrder(date, order).Order;
            bool expected = true;
            target.RemoveOrder(date, order.OrderNumber);

            //assert
            Assert.AreEqual(expected, actual.OrderCancelled);
        }

        [Test()]
        public void ShouldReturnResponseWithReviseOrder()
        {
            //arrange
            var target = new OrderOperations();
            var date = DateTime.Now.ToString("MMddyyyyy");
            var order = new Order
            {
                TaxInfo = new TaxInfo
                {
                    TaxRate = .1m,
                    StateAbbreviation = "OH"
                },
                ProdInfo = new ProductInfo
                {
                    ProductType = "Wood",
                    CostPerSquareFoot = 10,
                    LaborPerSquareFoot = 1
                },
                CustomerName = "Bob",
                Area = 10,
                OrderDate = DateTime.Now.Date
            };
            //act

            var actual = target.AddOrder(date, order);
            order.CustomerName = "Bill";
            target.ReviseOrder(date, order.OrderNumber, order);
            string expected = "Bill";

            //assert
            Assert.AreEqual(expected, actual.Order.CustomerName);
        }

        [Test()]
        public void ShouldReturnResponseWithTaxInfo()
        {
            var target = new OrderOperations();
            TaxInfo actual = new TaxInfo();
            TaxInfo expected = new TaxInfo()
            {
                StateAbbreviation = "OH",
                State = "OHIO",
                TaxRate = 0.0625m,
            };

            actual = target.GetTaxInfo("OH");

            Assert.AreEqual(expected.TaxRate, actual.TaxRate);
            Assert.AreEqual(expected.StateAbbreviation, actual.StateAbbreviation);
            Assert.AreEqual(expected.State, actual.State);
        }

        [Test()]
        public void ShouldReturnResponseWithProdInfo()
        {
            var target = new OrderOperations();
            ProductInfo actual = new ProductInfo();
            ProductInfo expected = new ProductInfo()
            {
                ProductType = "WOOD",
                CostPerSquareFoot = 5.15m,
                LaborPerSquareFoot = 4.75m,
            };
            actual = target.GetProductInfo("WOOD");

            Assert.AreEqual(expected.ProductType, actual.ProductType);
            Assert.AreEqual(expected.CostPerSquareFoot, actual.CostPerSquareFoot);
            Assert.AreEqual(expected.LaborPerSquareFoot, actual.LaborPerSquareFoot);
        }
    }
}