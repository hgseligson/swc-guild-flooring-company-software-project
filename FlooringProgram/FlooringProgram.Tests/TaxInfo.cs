﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models.Models
{
   public class TaxInfo
    {
        public string State { get; set; }

        public string StateAbbreviation { get; set; }

        public decimal TaxRate { get; set; }

    }
}
