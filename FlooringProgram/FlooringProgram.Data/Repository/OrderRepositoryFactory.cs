﻿using FlooringProgram.Models.Models;

namespace FlooringProgram.Data.Repository
{
    public class OrderRepositoryFactory
    {
        public static IRepository CreateOrderRepository(string repoType)
        {
            bool testRepo = repoType.Contains("Test");

            switch (testRepo)
            {
                case true:
                    return new TestRepository();

                default:
                    return new ProdRepository();
            }
        }
    }
}