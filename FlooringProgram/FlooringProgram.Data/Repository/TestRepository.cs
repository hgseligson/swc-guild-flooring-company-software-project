﻿using FlooringProgram.Models.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace FlooringProgram.Data.Repository
{
    public class TestRepository : IRepository
    {
        public Dictionary<string, List<Order>> Orders { get; set; }

        public TestRepository()
        {
            Dictionary<string, List<Order>> repository = new Dictionary<string, List<Order>>();
            string filePath = Environment.CurrentDirectory;
            string realFilePath = Path.GetFullPath(Path.Combine(filePath, @"..\..\"));
            IEnumerable<string> files = Directory.GetFiles(realFilePath + ConfigurationManager.AppSettings["FileName"], "Orders_*.txt", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                var fileReader = File.ReadAllLines(file);

                for (int i = 1; i < fileReader.Length; i++)
                {
                    var columns = fileReader[i].Split(',');
                    if (columns[2].Contains("$%^&"))
                    {
                        columns[2] = columns[2].Replace("$%^&", ",");
                    }
                    DateTime date = Convert.ToDateTime(columns[0]);
                    var order = new Order()
                    {
                        OrderDate = date,
                        OrderNumber = int.Parse(columns[1]),
                        CustomerName = columns[2],
                        Area = decimal.Parse(columns[3]),
                        OrderCancelled = bool.Parse(columns[10])
                    };
                    var tax = new TaxInfo()
                    {
                        State = columns[4],
                        StateAbbreviation = columns[5],
                        TaxRate = decimal.Parse(columns[6])
                    };
                    order.TaxInfo = tax;

                    var product = new ProductInfo()
                    {
                        ProductType = columns[7],
                        CostPerSquareFoot = decimal.Parse(columns[8]),
                        LaborPerSquareFoot = decimal.Parse(columns[9])
                    };

                    order.ProdInfo = product;

                    if (repository.ContainsKey(order.OrderDate.ToString("MMddyyy")))
                    {
                        repository[order.OrderDate.ToString("MMddyyy")].Add(order);
                    }
                    else
                    {
                        repository.Add(order.OrderDate.ToString("MMddyyy"), new List<Order>() { order });
                    }
                }
            }

            Orders = repository;
        }

        public string ConvertDateTimeToString(DateTime dateTime)
        {
            string returnDate = dateTime.ToString("MMddyyyy");

            return returnDate;
        }

        public List<Order> GetAll(string date)
        {
            if (Orders.ContainsKey(date))
            {
                return Orders[date];
            }
            return new List<Order>();
        }

        public Order GetById(string date, int orderId)
        {
            return GetAll(date).FirstOrDefault(o => o.OrderNumber == orderId);
        }

        public void UpdateOrder(string date, int orderId, Order updatedOrder)
        {
            for (int index = 0; index < Orders[date].Count; index++)
            {
                Order order = Orders[date][index];
                if (order.OrderNumber != orderId) continue;
                Orders[date][index] = updatedOrder;
                break;
            }

            Save();
        }

        //copy this into the prodrepo
        public int GenerateOrderNumber(string orderDate)
        {
            var orders = GetAll(orderDate);
            if (orders != null && orders.Any())
            {
                return orders.Max(o => o.OrderNumber) + 1;
            }
            return 1;
        }

        public Order CreateOrder(string date, Order createdOrder)
        {
            createdOrder.OrderNumber = GenerateOrderNumber(date);
            if (Orders.ContainsKey(date))
            {
                Orders[date].Add(createdOrder);
            }
            else
            {
                Orders.Add(date, new List<Order>() { createdOrder });
            }
            Save();
            return createdOrder;
        }

        public void DeleteOrder(string date, int orderId)
        {
            Order orderToRemove = GetById(date, orderId);
            orderToRemove.OrderCancelled = true;
            UpdateOrder(date, orderId, orderToRemove);
            Save();
        }

        public void Save()
        {
        }

        public void LogError(string error)
        {
            string filePath = Environment.CurrentDirectory;
            string realFilePath = Path.GetFullPath(Path.Combine(filePath, @"..\..\"));
            DateTime dtNow = DateTime.Now;
            string timeNow = dtNow.ToString("G");
            using (StreamWriter sw = File.AppendText(realFilePath + ConfigurationManager.AppSettings["FileName"] + "ErrorLog.txt"))
            {
                sw.WriteLine(timeNow + "   " + error);
            }
        }

        public List<TaxInfo> GetTaxInfo()
        {
            List<TaxInfo> listOfStateTaxes = new List<TaxInfo>();
            string filePath = Environment.CurrentDirectory;
            string realFilePath = Path.GetFullPath(Path.Combine(filePath, @"..\..\"));
            IEnumerable<string> files = Directory.GetFiles(realFilePath + ConfigurationManager.AppSettings["FileName"], "Taxes.txt", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                var fileReader = File.ReadAllLines(file);

                for (int i = 1; i < fileReader.Length; i++)
                {
                    var columns = fileReader[i].Split(',');

                    var newTaxInfo = new TaxInfo()
                    {
                        StateAbbreviation = columns[0].ToUpper(),
                        State = columns[1].ToUpper(),
                        TaxRate = decimal.Parse(columns[2])
                    };
                    listOfStateTaxes.Add(newTaxInfo);
                }
            }
            return listOfStateTaxes;
        }

        public List<ProductInfo> GetProductTypes()
        {
            List<ProductInfo> listOfProducts = new List<ProductInfo>();
            string filePath = Environment.CurrentDirectory;
            string realFilePath = Path.GetFullPath(Path.Combine(filePath, @"..\..\"));
            IEnumerable<string> files = Directory.GetFiles(realFilePath + ConfigurationManager.AppSettings["FileName"], "Products.txt", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                var fileReader = File.ReadAllLines(file);

                for (int i = 1; i < fileReader.Length; i++)
                {
                    var columns = fileReader[i].Split(',');

                    var productInfo = new ProductInfo()
                    {
                        ProductType = columns[0].ToUpper(),
                        CostPerSquareFoot = decimal.Parse(columns[1]),
                        LaborPerSquareFoot = decimal.Parse(columns[2])
                    };
                    listOfProducts.Add(productInfo);
                }
            }
            return listOfProducts;
        }

        public TaxInfo ReturnTaxInfo(string state)
        {
            TaxInfo taxInfo = new TaxInfo();
            var taxList = GetTaxInfo();
            foreach (var info in taxList)
            {
                if (info.StateAbbreviation == state)
                {
                    taxInfo = info;
                }
            }
            return taxInfo;
        }

        public ProductInfo ReturnProductInfo(string product)
        {
            ProductInfo productInfo = new ProductInfo();
            var productList = GetProductTypes();
            foreach (ProductInfo info in productList)
            {
                if (info.ProductType == product)
                {
                    productInfo = info;
                }
            }
            return productInfo;
        }
    }
}