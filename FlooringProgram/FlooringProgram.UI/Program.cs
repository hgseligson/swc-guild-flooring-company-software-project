﻿using FlooringProgram.UI.UIWorkflow;
using System;

namespace FlooringProgram.UI
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WindowWidth = 130;
            Console.WindowHeight = 43;

            MainMenu menu = new MainMenu();
            menu.PrintMainMenu();
        }
    }
}