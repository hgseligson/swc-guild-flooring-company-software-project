﻿using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using System;

namespace FlooringProgram.UI.UIWorkflow
{
    public class AddOrder
    {
        public AddOrder()
        {
            InputValidations validationConstruction = new InputValidations();
            Validator = validationConstruction;
            OrderOperations operationConstruction = new OrderOperations();
            OrderOps = operationConstruction;
        }

        public InputValidations Validator { get; set; }
        public OrderOperations OrderOps { get; set; }

        public void AddNewOrder()
        {
            TextPrompts printText = new TextPrompts();

            DateTime orderDate = printText.GetOrderDate();
            string customerName = CustomerName();
            string stateAbbrev = StateAbbrev();
            string productType = ProductType();
            decimal area = Area();
            TaxInfo taxInfo = OrderOps.GetTaxInfo(stateAbbrev);
            ProductInfo productInfo = OrderOps.GetProductInfo(productType);
            string orderDateString = Validator.ConvertDateTimeToString(orderDate);
            Order newOrder = new Order()
            {
                CustomerName = customerName,
                Area = area,
                ProdInfo = productInfo,
                TaxInfo = taxInfo,
                OrderDate = orderDate
            };
            string confirmation = printText.SaveNewOrderConfirmation();
            if (confirmation == "Y")
            {
                OrderResponse orderResponse = OrderOps.AddOrder(orderDateString, newOrder);
                if (orderResponse.Success)
                {
                    printText.ChangesSaved();
                    DisplayNewOrder(newOrder);
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("\n\tERROR: Order not saved!");
                    Console.WriteLine(orderResponse.Message);
                    Console.WriteLine("\n\tPress enter to proceed...");
                    Console.ReadLine();
                }
            }
            else if (confirmation == "N")
            {
                printText.ChangesDiscarded();
            }
        }

        public string CustomerName()
        {
            ValidateStringResponse customerNameValidation;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tPlease enter customers name: ");
                Console.ForegroundColor = ConsoleColor.White;
                string customerName = Console.ReadLine();
                customerNameValidation = Validator.CustomerNameValidation(customerName);

                if (customerNameValidation.Success == false)
                {
                    Console.Clear();
                    Console.WriteLine(customerNameValidation.Message);
                    Console.Write("\n\tPress enter to continue...");
                    Console.ReadLine();
                }
            } while (customerNameValidation.Success == false);

            return customerNameValidation.InputContent;
        }

        public string StateAbbrev()
        {
            ValidateStringResponse stateValidation;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tPlease enter state with the abbreviation: ");
                Console.ForegroundColor = ConsoleColor.White;
                string stateAbbrev = Console.ReadLine();
                stateValidation = Validator.StateValidation(stateAbbrev);
                if (stateValidation.Success == false)
                {
                    Console.Clear();
                    Console.WriteLine(stateValidation.Message);
                    Console.Write("\n\tPlease press enter to continue...");
                    Console.ReadLine();
                }
            } while (stateValidation.Success == false);
            return stateValidation.InputContent;
        }

        public string ProductType()
        {
            ValidateStringResponse productValidation;

            do
            {
                Console.Clear();
                Console.WriteLine("\n\tPlease enter the type of product purchased, product types available are...");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\t\tCarpet, Laminate, Tile, or Wood: ");
                Console.ForegroundColor = ConsoleColor.White;

                string productType = Console.ReadLine();

                productValidation = Validator.ProductValidation(productType);

                if (productValidation.Success == false)
                {
                    Console.Clear();
                    Console.WriteLine(productValidation.Message);
                    Console.Write("\n\tPlease press enter to continue...");
                    Console.ReadLine();
                }
            } while (productValidation.Success == false);
            return productValidation.InputContent;
        }

        public decimal Area()
        {
            ValidateDecimalResponse areaValidation;

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tPlease enter the area of flooring being purchased: ");
                Console.ForegroundColor = ConsoleColor.White;
                string area = Console.ReadLine();
                areaValidation = Validator.AreaValidation(area);

                if (areaValidation.Success == false)
                {
                    Console.Clear();
                    Console.WriteLine(areaValidation.Message);
                    Console.Write("\n\tPlease press enter to continue...");
                    Console.ReadLine();
                    Console.Clear();
                }
            } while (areaValidation.Success == false);

            return areaValidation.InputContent;
        }

        public void DisplayNewOrder(Order order)
        {
            TextPrompts printText = new TextPrompts();
            printText.PrintOrderDetails(order);
            Console.WriteLine("\n\tIf there are any errors please make corrections by choosing \"Edit Order\" from the Main Menu");
            Console.Write("\tPress enter when you are done reviewing this order...");
            Console.ReadLine();
        }
    }
}