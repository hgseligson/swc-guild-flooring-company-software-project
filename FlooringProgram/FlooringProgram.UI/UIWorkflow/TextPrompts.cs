﻿using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using System;
using System.Linq;

namespace FlooringProgram.UI.UIWorkflow
{
    public class TextPrompts
    {
        public TextPrompts()
        {
            InputValidations validationConstruction = new InputValidations();
            Validator = validationConstruction;
        }

        public InputValidations Validator { get; set; }

        public DateTime GetOrderDate()
        {
            ValidateDateTimeResponse dateResponse;
            do
            {
                Console.Clear();
                Console.Write("\n\tPlease enter the date of the order (mm/dd/yyyy): ");
                var input = Console.ReadLine();
                dateResponse = Validator.DateValidation(input);
                if (dateResponse.Success == false)
                {
                    Console.Clear();
                    Console.WriteLine(dateResponse.Message);
                    Console.Write("\n\tPlease press enter to continue...");
                    Console.ReadLine();
                }
            } while (dateResponse.Success == false);

            return dateResponse.InputContent;
        }

        public int SelectOrderNumber(OrderListResponse orderListResponse, DateTime orderDate, string workflow)
        {
            ValidateIntResponse orderNumbeResponse;
            do
            {
                Console.Clear();
                Console.WriteLine();
                foreach (var order in orderListResponse.OrdersForDate.Where(order => order.OrderCancelled == false))
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("\tOrder Number: ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("{0} - {1} - Total: {2:C}",order.OrderNumber, order.CustomerName, order.Total);
                }
                Console.Write("\n\tPlease select which order number from {0} you would like to {1}: ", orderDate.ToString("MM/dd/yyyy"),
                    workflow);
                string input = Console.ReadLine();

                orderNumbeResponse = Validator.OrderNumberValidation(input);

                if (orderNumbeResponse.Success == false)
                {
                    Console.Clear();
                    Console.WriteLine(orderNumbeResponse.Message);
                    Console.Write("\n\tPress enter to continue...");
                    Console.ReadLine();
                }
            } while (orderNumbeResponse.Success == false);

            return orderNumbeResponse.InputContent;
        }

        public string SaveRemovalConfirmation(string workflowType, int orderNumber)
        {
            string response;
            do
            {
                Console.WriteLine("\n\tAre you sure you would like to complete the {0} of Order Number: {1}?\n",
                    workflowType, orderNumber);
                Console.WriteLine("\tIMPORTANT: Choosing \"no\" will discard changes and return you to the Main Menu.");
                Console.WriteLine("\t           Choosing \"yes\" will save these changes!\n");
                Console.Write("\tPlease enter \"Y\" for yes or \"N\" for no: ");
                response = Console.ReadLine();
                if (response != null)
                {
                    response = response.ToUpper();
                }
                if ((response == null) || ((response != "Y") && (response != "N")))
                {
                    Console.WriteLine("\n\tThat is not a valid response, you must choose \"Y\" or \"N\"!");
                    Console.Write("\tPress enter to proceed...");
                    Console.Clear();
                }
            } while ((response != "Y") && (response != "N"));

            return response;
        }

        public void ChangesDiscarded()
        {
            Console.Clear();
            Console.WriteLine("\n\tNo changes were saved!\n");
            Console.Write("\tPress enter to return to the Main Menu...");
            Console.ReadLine();
        }

        public void ChangesSaved()
        {
            Console.Clear();
            Console.WriteLine("\n\tYour changes were saved!\n");
            Console.Write("\tPress enter to the proceed...");
            Console.ReadLine();
        }

        public void PrintOrderDetails(Order selectedOrder)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\n\tOrder Number: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0}", selectedOrder.OrderNumber);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tOrder Date: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0}", selectedOrder.OrderDate.ToShortDateString());
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tCustomer Name: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0}", selectedOrder.CustomerName.Replace("$%^&", ","));
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tDimensions: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0} Sq. Ft.", selectedOrder.Area);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tProduct Type: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0}", selectedOrder.ProdInfo.ProductType);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tCost Per SqFt: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0:C}", selectedOrder.ProdInfo.CostPerSquareFoot);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tMaterial Cost: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0:C}", selectedOrder.MaterialCost);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tLabor Per SqFt: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0:C}", selectedOrder.ProdInfo.LaborPerSquareFoot);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tLabor Cost: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0:C}", selectedOrder.LaborCost);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tTax: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0:C}", selectedOrder.Tax);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\tTotal: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0:C}", selectedOrder.Total);
            Console.Write("\n\t");
        }

        public string SaveNewOrderConfirmation()
        {
            string response;
            do
            {
                Console.WriteLine("\n\tAre you sure you would like to complete the adding of this new order?\n");
                Console.WriteLine("\tIMPORTANT: Choosing \"no\" will discard changes and return you to the Main Menu.");
                Console.WriteLine("\t           Choosing \"yes\" will save these changes!\n");
                Console.Write("\tPlease enter \"Y\" for yes or \"N\" for no: ");
                response = Console.ReadLine();
                if (response != null)
                {
                    response = response.ToUpper();
                }
                if ((response == null) || ((response != "Y") && (response != "N")))
                {
                    Console.WriteLine("\nThat is not a valid response, you must choose \"Y\" or \"N\"!");
                    Console.Write("Press enter to proceed...");
                    Console.Clear();
                }
            } while ((response != "Y") && (response != "N"));

            return response;
        }

        public int SelectRemovedOrderNumber(OrderListResponse orderListResponse, DateTime orderDate, string workflow)
        {
            ValidateIntResponse orderNumbeResponse;
            do
            {
                Console.Clear();
                Console.WriteLine();
                foreach (var order in orderListResponse.OrdersForDate.Where(order => order.OrderCancelled))
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("\tOrder Number: ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("{0}- {1}-Total: {2:C}",order.OrderNumber, order.CustomerName, order.Total);
                }
                Console.Write("\n\tPlease select which removed order number from {0} you would like to {1}: ", orderDate.ToString("MM/dd/yyyy"),
                    workflow);
                string input = Console.ReadLine();

                orderNumbeResponse = Validator.OrderNumberValidation(input);

                if (orderNumbeResponse.Success == false)
                {
                    Console.Clear();
                    Console.WriteLine(orderNumbeResponse.Message);
                    Console.Write("\n\tPress enter to continue...");
                    Console.ReadLine();
                }
            } while (orderNumbeResponse.Success == false);

            return orderNumbeResponse.InputContent;
        }

        public string SaveChangesConfirmation()
        {
            string response;
            do
            {
                Console.WriteLine("\n\tAre you sure you would like to save these changes?\n");
                Console.WriteLine("\tIMPORTANT: Choosing \"no\" will discard changes and return you to the Main Menu.");
                Console.WriteLine("\t           Choosing \"yes\" will save these changes!\n");
                Console.Write("\tPlease enter \"Y\" for yes or \"N\" for no: ");
                response = Console.ReadLine();
                if (response != null)
                {
                    response = response.ToUpper();
                }
                if ((response == null) || ((response != "Y") && (response != "N")))
                {
                    Console.WriteLine("\n\tThat is not a valid response, you must choose \"Y\" or \"N\"!");
                    Console.Write("\tPress enter to proceed...");
                    Console.Clear();
                }
            } while ((response != "Y") && (response != "N"));

            return response;
        }
    }
}