﻿using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using System;

namespace FlooringProgram.UI.UIWorkflow
{
    public class RemoveOrder

    {
        public static void OrderRemoval()
        {
            InputValidations removeOrderValidation = new InputValidations();
            TextPrompts textPrompts = new TextPrompts();

            DateTime processOrderDate = textPrompts.GetOrderDate();

            OrderOperations orderOp = new OrderOperations();

            string orderDate = removeOrderValidation.ConvertDateTimeToString(processOrderDate);

            OrderListResponse orderListResponse = orderOp.GetOrderList(orderDate);

            if (orderListResponse.Success)
            {
                int orderNumber = textPrompts.SelectOrderNumber(orderListResponse, processOrderDate, "display");
                OrderResponse orderResponse = orderOp.GetOrder(orderDate, orderNumber);

                if (orderResponse.Success && !orderResponse.Order.OrderCancelled)
                {
                    textPrompts.PrintOrderDetails(orderResponse.Order);
                    string confirmation = textPrompts.SaveRemovalConfirmation("removal", orderResponse.Order.OrderNumber);

                    if (confirmation == "Y")
                    {
                        //bll layer call

                        orderOp.RemoveOrder(orderDate, orderNumber);
                        textPrompts.ChangesSaved();
                    }
                    else
                    {
                        //don't save changes to data base
                        textPrompts.ChangesDiscarded();
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("\n\t{0}", orderResponse.Message);
                    Console.Write("\n\tPress enter to proceed...");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\n\t{0}", orderListResponse.Message);
                Console.Write("\n\tPress enter to proceed...");
                Console.ReadLine();
            }
        }
    }
}