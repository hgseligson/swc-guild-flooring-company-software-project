﻿using System;

namespace FlooringProgram.UI.UIWorkflow
{
    public class MainMenu
    {
        public void PrintMainMenu()
        {
            string input;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(@"
           _____                      _   _                     _           _
          |  ___| __ ___  _ __ ___   | |_| |__   ___  __      _(_)_ __   __| | _____      _____
          | |_ | '__/ _ \| '_ ` _ \  | __| '_ \ / _ \ \ \ /\ / / | '_ \ / _` |/ _ \ \ /\ / / __|
          |  _|| | | (_) | | | | | | | |_| | | |  __/  \ V  V /| | | | | (_| | (_) \ V  V /\__ \_ _ _
          |_|  |_|  \___/|_| |_| |_|  \__|_| |_|\___|_ _\_/\_/ |_|_| |_|\__,_|\___/ \_/\_/ |___(_|_|_)
           _          _   _                          _ _
          | |_ ___   | |_| |__   ___  __      ____ _| | |___
          | __/ _ \  | __| '_ \ / _ \ \ \ /\ / / _` | | / __|
          | || (_) | | |_| | | |  __/  \ V  V / (_| | | \__ \_ _ _
           \__\___/   \__|_| |_|\___|   \_/\_/ \__,_|_|_|___(_|_|_)

             __    _ __       __               ________                 _
            / /   (_) /      / /___  ____     / ____/ /___  ____  _____(_)___  ____ _
           / /   / / /  __  / / __ \/ __ \   / /_  / / __ \/ __ \/ ___/ / __ \/ __ `/
          / /___/ / /  / /_/ / /_/ / / / /  / __/ / / /_/ / /_/ / /  / / / / / /_/ /
         /_____/_/_/   \____/\____/_/ /_/  /_/   /_/\____/\____/_/  /_/_/ /_/\__, /
                                                                            /____/

             __                           __          __                                              __   __
            / /_  ____ ______   _      __/ /_  ____ _/ /_   __  ______  __  __   ____  ___  ___  ____/ /  / /
           / __ \/ __ `/ ___/  | | /| / / __ \/ __ `/ __/  / / / / __ \/ / / /  / __ \/ _ \/ _ \/ __  /  / /
          / / / / /_/ (__  )   | |/ |/ / / / / /_/ / /_   / /_/ / /_/ / /_/ /  / / / /  __/  __/ /_/ /  /_/
         /_/ /_/\__,_/____/    |__/|__/_/ /_/\__,_/\__/   \__, /\____/\__,_/  /_/ /_/\___/\___/\__,_/  (_)
                                                         /____/
            ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\t\t\t\t\t\t\t\t\ta subsidiary of SG Corp.");

                Console.WriteLine("\n\t1. Displays Orders");
                Console.WriteLine("\t2. Add Order");
                Console.WriteLine("\t3. Edit Order");
                Console.WriteLine("\t4. Remove Order");
                Console.WriteLine("\t5. View Removed Orders");
                Console.WriteLine("\n\tQ. Quit");
                Console.Write("\n\tPlease enter your choice: ");
                input = Console.ReadLine();

                if (input != null && input.ToUpper() != "Q")
                {
                    ProcessChoice(input);
                }
            } while (input != null && input.ToUpper() != "Q");
        }

        private static void ProcessChoice(string choice)
        {
            AddOrder newOrder = new AddOrder();
            EditOrder editOrder = new EditOrder();
            switch (choice)
            {
                case "1":
                    DisplayOrder.OrderDisplay();
                    break;

                case "2":
                    newOrder.AddNewOrder();
                    break;

                case "3":
                    editOrder.DisplayOrderForEdit();
                    break;

                case "4":
                    RemoveOrder.OrderRemoval();
                    break;

                case "5":
                    ViewRemovedOrders.RemovedOrderDisplay();
                    break;

                default:
                    Console.WriteLine("\n\tThat is not a valid option. Please choose another option!");
                    Console.Write("\tPress enter to proceed...");
                    Console.ReadLine();
                    break;
            }
        }
    }
}