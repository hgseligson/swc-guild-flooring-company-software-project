﻿using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using System;

namespace FlooringProgram.UI.UIWorkflow
{
    public class DisplayOrder
    {
        public static void OrderDisplay()
        {
            InputValidations displayOrderValidation = new InputValidations();
            TextPrompts textPrompts = new TextPrompts();

            DateTime processOrderDate = textPrompts.GetOrderDate();

            OrderOperations orderOp = new OrderOperations();

            string orderDate = displayOrderValidation.ConvertDateTimeToString(processOrderDate);

            OrderListResponse orderListResponse = orderOp.GetOrderList(orderDate);
            if (orderListResponse.Success)
            {
                int orderNumber = textPrompts.SelectOrderNumber(orderListResponse, processOrderDate, "display");
                OrderResponse orderResponse = orderOp.GetOrder(orderDate, orderNumber);

                if (orderResponse.Success && !orderResponse.Order.OrderCancelled)
                {
                    textPrompts.PrintOrderDetails(orderResponse.Order);
                    Console.Write("Press enter when you are done viewing this order...");
                    Console.ReadLine();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("\n\t{0}", orderResponse.Message);
                    Console.Write("\n\tPress enter to proceed...");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\n\t{0}", orderListResponse.Message);
                Console.Write("\n\tPress enter to proceed...");
                Console.ReadLine();
            }
        }
    }
}