﻿using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using System;

namespace FlooringProgram.UI.UIWorkflow
{
    public class ViewRemovedOrders
    {
        public static void RemovedOrderDisplay()
        {
            InputValidations displayOrderValidation = new InputValidations();
            TextPrompts textPrompts = new TextPrompts();

            DateTime processOrderDate = textPrompts.GetOrderDate();

            OrderOperations orderOp = new OrderOperations();

            string orderDate = displayOrderValidation.ConvertDateTimeToString(processOrderDate);

            OrderListResponse orderListResponse = orderOp.GetOrderList(orderDate);
            if (orderListResponse.OrdersForDate != null)
            {
                int orderNumber = textPrompts.SelectRemovedOrderNumber(orderListResponse, processOrderDate, "display");
                OrderResponse orderResponse = orderOp.GetOrder(orderDate, orderNumber);

                if (orderResponse.Success && orderResponse.Order.OrderCancelled)
                {
                    textPrompts.PrintOrderDetails(orderResponse.Order);
                    Console.Write("Press enter when you are done viewing this order...");
                    Console.ReadLine();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("\n\tThe order number you entered either does not exist or has not been cancelled.");
                    Console.Write("\n\tPress enter to proceed...");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\n\tThe date you entered has no orders that were cancelled.");
                Console.Write("\n\tPress enter to proceed...");
                Console.ReadLine();
            }
        }
    }
}