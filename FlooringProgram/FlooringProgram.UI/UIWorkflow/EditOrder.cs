﻿using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using System;

namespace FlooringProgram.UI.UIWorkflow
{
    public class EditOrder
    {
        public EditOrder()
        {
            InputValidations validationConstruction = new InputValidations();
            Validator = validationConstruction;
            OrderOperations operationConstruction = new OrderOperations();
            OrderOps = operationConstruction;
        }

        public InputValidations Validator { get; set; }
        public OrderOperations OrderOps { get; set; }

        public void DisplayOrderForEdit()
        {
            TextPrompts printText = new TextPrompts();

            DateTime orderDate = printText.GetOrderDate();

            string orderDateString = Validator.ConvertDateTimeToString(orderDate);

            OrderListResponse orderListResponse = OrderOps.GetOrderList(orderDateString);

            if (orderListResponse.Success)
            {
                int orderNumber = printText.SelectOrderNumber(orderListResponse, orderDate, "edit");
                OrderResponse orderResponse = OrderOps.GetOrder(orderDateString, orderNumber);

                if (orderResponse.Success && !orderResponse.Order.OrderCancelled)
                {
                    printText.PrintOrderDetails(orderResponse.Order);
                    Console.Write("\n\tPlease press enter when you are ready to edit the order...");
                    Console.ReadLine();
                    Console.WriteLine("\n\tYou may edit each field of the order.  Press enter to continue");

                    //values to pass to edit

                    orderResponse.Order.OrderDate = EditOrderDate(orderResponse.Order.OrderDate);
                    orderResponse.Order.CustomerName = EditCustomerName(orderResponse.Order.CustomerName);
                    orderResponse.Order.Area = EditArea(orderResponse.Order.Area);
                    orderResponse.Order.ProdInfo.ProductType = EditProduct(orderResponse.Order.ProdInfo.ProductType);
                    orderResponse.Order.ProdInfo.CostPerSquareFoot = EditCostPerSquareFoot(orderResponse.Order.ProdInfo.CostPerSquareFoot);
                    orderResponse.Order.ProdInfo.LaborPerSquareFoot = EditLaborPerSquareFoot(orderResponse.Order.ProdInfo.LaborPerSquareFoot);
                    orderResponse.Order.TaxInfo.TaxRate = EditTax(orderResponse.Order.TaxInfo.TaxRate);

                    TaxInfo editedTaxInfo = new TaxInfo()
                    {
                        State = orderResponse.Order.TaxInfo.State,
                        StateAbbreviation = orderResponse.Order.TaxInfo.StateAbbreviation,
                        TaxRate = orderResponse.Order.TaxInfo.TaxRate
                    };

                    ProductInfo editedProductInfo = new ProductInfo()
                    {
                        ProductType = orderResponse.Order.ProdInfo.ProductType,
                        CostPerSquareFoot = orderResponse.Order.ProdInfo.CostPerSquareFoot,
                        LaborPerSquareFoot = orderResponse.Order.ProdInfo.LaborPerSquareFoot
                    };

                    Order editedOrder = new Order()
                    {
                        CustomerName = orderResponse.Order.CustomerName,
                        Area = orderResponse.Order.Area,
                        ProdInfo = editedProductInfo,
                        TaxInfo = editedTaxInfo,
                        OrderDate = orderResponse.Order.OrderDate
                    };

                    orderDateString = Validator.ConvertDateTimeToString(editedOrder.OrderDate);
                    Console.Clear();
                    string confirmation = printText.SaveChangesConfirmation();

                    if (confirmation == "Y")
                    {
                        printText.ChangesSaved();
                        int x = orderListResponse.OrdersForDate.FindIndex(o => o.OrderNumber == orderResponse.Order.OrderNumber);
                        if (orderListResponse.OrdersForDate[x].OrderDate != editedOrder.OrderDate)
                        {
                            orderListResponse.OrdersForDate[x] = null;
                            orderResponse.Order = null;
                            OrderOps.AddOrder(orderDateString, editedOrder);
                            printText.PrintOrderDetails(editedOrder);
                        }
                        else
                        {
                            OrderOps.ReviseOrder(orderDateString, orderResponse.Order.OrderNumber, orderResponse.Order);
                            printText.PrintOrderDetails(orderResponse.Order);
                        }

                        Console.Write("\n\tPress Enter when you are finished viewing your changes...");
                        Console.ReadLine();
                    }
                    else
                    {
                        printText.ChangesDiscarded();
                    }
                }

                else
                {
                    Console.Clear();
                    Console.WriteLine("\n\t{0}", orderResponse.Message);
                    Console.Write("\n\tPress enter to proceed ...");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\n\t{0}", orderListResponse.Message);
                Console.Write("\n\tPress enter to proceed ...");
                Console.ReadLine();
            }
        }

        //methods to edit order information and return revised information

        public DateTime EditOrderDate(DateTime orderDate)
        {
            ValidateDateTimeResponse dateResponse;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tOrder Date: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(orderDate.ToString("MM/dd/yyyy"));
                Console.Write("\n\tIf the current date is OK, press Enter to move to the next field...\n\n\tPlease enter the revised order date (mm/dd/yyyy): ");
                string editedDate = Console.ReadLine();
                dateResponse = Validator.DateValidation(editedDate);
                if (!dateResponse.Success)
                {
                    editedDate = editedDate.Trim();
                    if (editedDate == "")
                    {
                        return orderDate;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("\n\tERROR: Please enter the order date in the correct format");
                        Console.Write("\n\tPlease press enter to continue...");
                        Console.ReadLine();
                    }
                }
            } while (dateResponse.Success == false);

            return dateResponse.InputContent;
        }

        public string EditCustomerName(string custName)
        {
            ValidateStringResponse nameValidation;
            string editedName;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tCustomer Name: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(custName);
                Console.Write(
                    "\n\tIf the current customer name is OK, press Enter to move to the next field...\n\n\tPlease enter the revised Customer Name: ");
                editedName = Console.ReadLine();
                nameValidation = Validator.CustomerNameValidation(editedName);
                if (editedName == "")
                {
                    return custName;
                }
                if (!nameValidation.Success)
                {
                    Console.Clear();
                    Console.WriteLine(nameValidation.Message);
                    Console.Write("\n\tPlease press enter to continue ...");
                    Console.ReadLine();
                    Console.Clear();
                }
            } while (editedName != "" && !nameValidation.Success); 
            return editedName;
        }

        public decimal EditArea(decimal area)
        {
            ValidateDecimalResponse decimalResponse;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tOrder Area: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("{0} Sq. Ft.",area);
                Console.Write(
                    "\n\tIf the current order area is OK, press Enter to move to the next field...\n\n\tPlease enter the revised Order Area: ");
                string editedArea = Console.ReadLine();

                decimalResponse = Validator.AreaValidation(editedArea);
                if (!decimalResponse.Success)
                {
                    if (editedArea == "")
                    {
                        return area;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine(decimalResponse.Message);
                        Console.Write("\n\tPlease press enter to continue ...");
                        Console.ReadLine();
                        Console.Clear();
                    }
                }
            } while (!decimalResponse.Success);
            return decimalResponse.InputContent;
        }

        public string EditProduct(string product)
        {
            ValidateStringResponse stringResponse;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tProduct Type: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(product);
                Console.Write(
                    "\n\tIf the current product type is OK, press Enter to move to the next field...\n\n\tPlease enter the revised Product Type: ");
                string editedProduct = Console.ReadLine();
                stringResponse = Validator.ProductValidation(editedProduct);
                if (!stringResponse.Success)
                {
                    if (editedProduct == "")
                    {
                        return product;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine(stringResponse.Message);
                        Console.Write("\n\tPlease press enter to continue ...");
                        Console.ReadLine();
                    }
                }
            } while (!stringResponse.Success);
            return stringResponse.InputContent;
        }

        public decimal EditCostPerSquareFoot(decimal squareFootCost)
        {
            ValidateDecimalResponse squareFootResponse;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tCost per square foot: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("{0:c}", squareFootCost);
                Console.Write(
                    "\n\tIf the current cost per square foot is OK, press Enter to move to the next field...\n\n\tPlease enter the revised Cost Per Square Foot: ");
                string editedCost = Console.ReadLine();
                squareFootResponse = Validator.SquareFootageValidation(editedCost);
                if (!squareFootResponse.Success)
                {
                    if (editedCost == "")
                    {
                        return squareFootCost;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine(squareFootResponse.Message);
                        Console.Write("\n\tPress enter to continue ...");
                        Console.ReadLine();
                        Console.Clear();
                    }
                }
            } while (!squareFootResponse.Success);

            return squareFootResponse.InputContent;
        }

        public decimal EditLaborPerSquareFoot(decimal laborFootCost)
        {
            ValidateDecimalResponse squareFootResponse;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tLabor per square foot: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("{0:c}",laborFootCost);
                Console.Write(
                    "\n\tIf the current labor per square foot is OK, press Enter to move to the next field...\n\n\tPlease enter the revised Labor Per Square Foot: ");
                string editedLabor = Console.ReadLine();
                squareFootResponse = Validator.SquareFootageValidation(editedLabor);
                if (!squareFootResponse.Success)
                {
                    if (editedLabor == "")
                    {
                        return laborFootCost;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine(squareFootResponse.Message);
                        Console.Write("\n\tPress enter to continue ...");
                        Console.ReadLine();
                        Console.Clear();
                    }
                }
            } while (!squareFootResponse.Success);

            return squareFootResponse.InputContent;
        }

        public decimal EditTax(decimal tax)
        {
            ValidateDecimalResponse taxResponse;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n\tTax Rate: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("{0:F}%", tax * 100);
                Console.Write(
                    "\n\tIf the current Tax Rate is OK, press Enter to move to the next field...\n\n\tPlease enter the revised Tax Rate: ");
                string editedTax = Console.ReadLine();

                taxResponse = Validator.TaxValidation(editedTax);
                if (!taxResponse.Success)
                {
                    if (editedTax == "")
                    {
                        return tax;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine(taxResponse.Message);
                        Console.Write("\n\tPlease press enter to continue ...");
                        Console.ReadLine();
                        Console.Clear();
                    }
                }
            } while (!taxResponse.Success);
            return taxResponse.InputContent;
        }
    }
}