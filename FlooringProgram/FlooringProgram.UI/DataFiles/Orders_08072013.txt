Date, OrdNum, CustName, Area, State, StateAbbrev, TaxRate, ProdType, CostPerSqFt, LaborPerSqFt, OrderCancelled
08/07/2013,1,Hannah's Homes,4230,PA,Pennsylvania,0.0675,Laminate,1.75,2.1,True
08/07/2013,2,Bill's Floors,9340,IN,Indiana,0.06,Tile,3.5,4.15,False
08/07/2013,3,Sarah's Interior Design,8230,MI,Michigan,0.0575,Carpet,2.25,2.1,False
08/07/2013,4,The Flooring Experts,6830,MI,Michigan,0.0575,Carpet,2.25,2.1,True
08/07/2013,5,Flooring Solutions,8400,MI,Michigan,0.0575,Carpet,2.25,2.1,True
