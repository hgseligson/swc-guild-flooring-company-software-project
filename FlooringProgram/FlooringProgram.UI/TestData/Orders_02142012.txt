﻿Date, OrdNum, CustName, Area, State, StateAbbrev, TaxRate, ProdType, CostPerSqFt, LaborPerSqFt, OrderCancelled
02/14/2012,1,Cupid's Floors and Remodeling,5470,MI,Michigan,0.0575,Laminate,1.75,2.1,True
02/14/2012,2,Forever Floors,8420,IN,Indiana,0.06,Laminate,1.75,2.1,False
02/14/2012,3,The Floor Authority,7420,IN,Indiana,0.06,Carpet,2.25,2.1,True
02/14/2012,4,Floors2U,6280,OH,Ohio,0.0625,Carpet,2.25,2.1,False
02/14/2012,5,Love Flooring,9320,PA,Pennsylvania,0.0675,Tile,3.5,4.15,True