﻿using FlooringProgram.Data.Repository;
using FlooringProgram.Models.Models;
using System;
using System.Configuration;

namespace FlooringProgram.BLL
{
    public class OrderOperations
    {
        public OrderOperations()
        {
            string repoType = ConfigurationManager.AppSettings["FileName"];
            var repoCreation = OrderRepositoryFactory.CreateOrderRepository(repoType);
            DataRepository = repoCreation;
        }

        public IRepository DataRepository { get; set; }

        public OrderResponse AddOrder(string date, Order newOrder)
        {
            OrderResponse returnResponse = new OrderResponse();
            string backUpOrder = "";
            try
            {
                Order createdOrder = DataRepository.CreateOrder(date, newOrder);

                returnResponse.Order = createdOrder;
                returnResponse.Success = true;
                if (newOrder != null && newOrder.CustomerName != null && newOrder.TaxInfo.StateAbbreviation != null && newOrder.ProdInfo.ProductType != null)
                {
                    backUpOrder = "User Entry--CustName: " + newOrder.CustomerName + "State: " + newOrder.TaxInfo.StateAbbreviation +
                                  "Product: " + newOrder.ProdInfo.ProductType + "Area: " + newOrder.Area;
                }
            }
            catch (Exception ex)
            {
                returnResponse.Order = null;
                returnResponse.Success = false;
                returnResponse.Message = "\n\tUnable to create new order, please contact your Data Base Admin.";
                string logErrorMsg = "Error: new order not created     " + backUpOrder;
                DataRepository.LogError(logErrorMsg);
            }
            return returnResponse;
        }

        public OrderResponse GetOrder(string date, int orderId)
        {
            OrderResponse returnResponse = new OrderResponse();

            try
            {
                Order order = DataRepository.GetById(date, orderId);
                returnResponse.Order = order;
                returnResponse.Success = true;
                if (returnResponse.Order.OrderCancelled)
                {
                    returnResponse.Message = "No order exists for that order number.\n\n\t" +
                                             "It is possible this order was removed." +
                                             "\n\tRemoved orders can be viewed by selecting \"View Removed Orders\" from the Main Menu.";
                }
            }
            catch (Exception)
            {
                returnResponse.Order = null;
                returnResponse.Success = false;
                returnResponse.Message = "No order exists for that order number.";
                string logErrorMsg = "Error: order didn't exist" + "   User Entry--" + "Date: " + date + " OrderNum: " + orderId;
                DataRepository.LogError(logErrorMsg);
            }
            return returnResponse;
        }

        public OrderListResponse GetOrderList(string orderDate)
        {
            OrderListResponse returnResponse = new OrderListResponse();
            var orders = DataRepository.GetAll(orderDate);
            if (orders != null && orders.Count > 0)
            {
                returnResponse.OrdersForDate = orders;
                returnResponse.Success = true;
                int orderNotCancelled = 0;
                foreach (var order in returnResponse.OrdersForDate)
                {
                    if (!order.OrderCancelled)
                    {
                        orderNotCancelled++;
                    }
                }
                if (orderNotCancelled == 0)
                {
                    returnResponse.Success = false;
                    returnResponse.Message = "No orders were found for that date.\n\n\t" +
                                             "It is possible that orders for this date were removed." +
                                             "\n\tRemoved orders can be viewed by selecting \"View Removed Orders\" from the Main Menu.";
                }
            }
            else
            {
                returnResponse.OrdersForDate = null;
                returnResponse.Success = false;
                returnResponse.Message = "No orders were found for that date.";
                string logErrorMsg = "Error: no orders for date found    User Entry: " + orderDate;
                DataRepository.LogError(logErrorMsg);
            }
            return returnResponse;
        }

        public OrderResponse RemoveOrder(string date, int orderId)
        {
            OrderResponse returnResponse = new OrderResponse();

            try
            {
                Order order = DataRepository.GetById(date, orderId);
                if (order == null)
                {
                    returnResponse.Order = null;
                    returnResponse.Success = false;
                    returnResponse.Message = "\n\tNo order exists for that order number.";
                }
                else
                {
                    DataRepository.DeleteOrder(date, orderId);
                    returnResponse.Order = order;
                    returnResponse.Success = true;
                }
            }
            catch (Exception)
            {
                returnResponse.Order = null;
                returnResponse.Success = false;
                returnResponse.Message = "\n\tUnable to remove order, please contact your Data Base Admin.";
                string logErrorMsg = "Error: unable to remove order" + "   User Entry--" + "Date: " + date + " OrderNum: " + orderId;
                DataRepository.LogError(logErrorMsg);
            }
            return returnResponse;
        }

        public OrderResponse ReviseOrder(string date, int orderId, Order updatedOrder)
        {
            OrderResponse returnResponse = new OrderResponse();
            string backUpOrder = "";

            try
            {
                Order order = DataRepository.GetById(date, orderId);
                if (order == null)
                {
                    returnResponse.Order = null;
                    returnResponse.Success = false;
                    returnResponse.Message = "\n\t order exists for that order number.";
                }
                else
                {
                    DataRepository.UpdateOrder(date, orderId, updatedOrder);
                    returnResponse.Order = order;
                    returnResponse.Success = true;
                }
                if (updatedOrder != null && updatedOrder.CustomerName != null && updatedOrder.TaxInfo.StateAbbreviation != null && updatedOrder.ProdInfo.ProductType != null)
                {
                    backUpOrder = "User Entry--CustName: " + updatedOrder.CustomerName + "State: " + updatedOrder.TaxInfo.StateAbbreviation +
                                  "Product: " + updatedOrder.ProdInfo.ProductType + "Area: " + updatedOrder.Area;
                }
            }
            catch (Exception ex)
            {
                returnResponse.Order = null;
                returnResponse.Success = false;
                returnResponse.Message = "\n\tUnable to save edit changes, please contact your Data Base Admin.";
                string logErrorMsg = "Error: new order not created     " + backUpOrder;
                DataRepository.LogError(logErrorMsg);
            }
            return returnResponse;
        }

        public TaxInfo GetTaxInfo(string state)
        {
            TaxInfo taxInfo = DataRepository.ReturnTaxInfo(state);
            return taxInfo;
        }

        public ProductInfo GetProductInfo(string product)
        {
            ProductInfo productInfo = DataRepository.ReturnProductInfo(product);
            return productInfo;
        }
    }
}