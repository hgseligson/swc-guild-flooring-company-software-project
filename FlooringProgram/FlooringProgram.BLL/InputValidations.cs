﻿using FlooringProgram.Data.Repository;
using FlooringProgram.Models.Models;
using System;
using System.Configuration;

namespace FlooringProgram.BLL
{
    public class InputValidations
    {
        public InputValidations()
        {
            string repoType = ConfigurationManager.AppSettings["FileName"];
            var repoCreation = OrderRepositoryFactory.CreateOrderRepository(repoType);
            DataRepository = repoCreation;
        }

        public IRepository DataRepository { get; set; }

        public ValidateDateTimeResponse DateValidation(string date)
        {
            ValidateDateTimeResponse dateValidation = new ValidateDateTimeResponse();
            DateTime orderDate;
            try
            {
                orderDate = DateTime.Parse(date);
            }
            catch (Exception)
            {
                dateValidation.Message = "\n\tERROR: Please enter the order date in the correct format!";
                string logErrorMsg = "Error: incorrect date" + "   User Entry: " + date;
                DataRepository.LogError(logErrorMsg);
                return dateValidation;
            }
            dateValidation.Success = true;
            dateValidation.InputContent = orderDate;
            return dateValidation;
        }

        public ValidateIntResponse OrderNumberValidation(string orderNum)
        {
            ValidateIntResponse orderNumValidation = new ValidateIntResponse();
            int orderNumber;
            try
            {
                orderNumber = int.Parse(orderNum);
            }
            catch (Exception)
            {
                orderNumValidation.Message = "\n\tERROR: That is not a valid numeric entry!";
                string logErrorMsg = "Error: wrong order number" + "   User Entry: " + orderNum;
                DataRepository.LogError(logErrorMsg);
                return orderNumValidation;
            }
            orderNumValidation.Success = true;
            orderNumValidation.InputContent = orderNumber;
            return orderNumValidation;
        }

        public ValidateStringResponse CustomerNameValidation(string custName)
        {
            ValidateStringResponse custNameValidation = new ValidateStringResponse();
            custName = custName.Trim();
            if (custName == "")
            {
                custNameValidation.Message = "\n\tERROR: Customer's name must be at least one character!";
                string logErrorMsg = "Error: no customer name" + "   User Entry: " + "empty string";
                DataRepository.LogError(logErrorMsg);
                return custNameValidation;
            }
            custNameValidation.Success = true;
            custNameValidation.InputContent = custName;
            return custNameValidation;
        }

        public ValidateStringResponse StateValidation(string state)
        {
            ValidateStringResponse stateValidation = new ValidateStringResponse();
            state = state.ToUpper();
            var taxList = DataRepository.GetTaxInfo();

            foreach (var taxInfo in taxList)
            {
                if (taxInfo.StateAbbreviation == state)
                {
                    stateValidation.Success = true;
                    stateValidation.InputContent = state;
                    return stateValidation;
                }
            }
            stateValidation.Message = "\n\tERROR: We only service the following states: OH, PA, MI, & IN";
            string logErrorMsg = "Error: wrong state" + "   User Entry: " + state;
            DataRepository.LogError(logErrorMsg);
            return stateValidation;
        }

        public ValidateStringResponse ProductValidation(string productType)
        {
            ValidateStringResponse productValidation = new ValidateStringResponse();
            productType = productType.ToUpper();
            var productList = DataRepository.GetProductTypes();

            foreach (var productInfo in productList)
            {
                if (productInfo.ProductType == productType)
                {
                    productValidation.Success = true;
                    productValidation.InputContent = productType;
                    return productValidation;
                }
            }
            productValidation.Message = "\n\tERROR: We only offer the following flooring materials..." +
                                        "\n\t\tCarpet, Laminate, Tile, & Wood";
            string logErrorMsg = "Error: wrong product" + "   User Entry: " + productType;
            DataRepository.LogError(logErrorMsg);
            return productValidation;
        }

        public ValidateDecimalResponse AreaValidation(string area)
        {
            ValidateDecimalResponse areaValidation = new ValidateDecimalResponse();
            string negArea = "ERROR: Area can not be a negative number!";
            decimal areaDecimal;
            try
            {
                areaDecimal = decimal.Parse(area);
                if (areaDecimal < 1)
                {
                    throw new SystemException(negArea);
                }
            }
            catch (Exception e)
            {
                areaValidation.Message = "\n\tERROR: That is not a valid entry for area!";
                if (e.Message == negArea)
                {
                    areaValidation.Message = "\n\t" + negArea;
                }
                string logErrorMsg = "Error: area neg/zero" + "   User Entry: " + area;
                DataRepository.LogError(logErrorMsg);
                return areaValidation;
            }
            areaValidation.Success = true;
            areaValidation.InputContent = areaDecimal;
            return areaValidation;
        }

        public string ConvertDateTimeToString(DateTime dateTime)
        {
            string returnDate = dateTime.ToString("MMddyyyy");

            return returnDate;
        }

        public ValidateDecimalResponse SquareFootageValidation(string squareFootage)
        {
            ValidateDecimalResponse squareFootageValidation = new ValidateDecimalResponse();
            string negSquareFootage = "\n\tERROR: Square footage can not be a negative number!";
            decimal squareFootageDecimal;
            try
            {
                squareFootageDecimal = decimal.Parse(squareFootage);
                if (squareFootageDecimal <= 0)
                {
                    throw new SystemException(negSquareFootage);
                }
            }
            catch (Exception e)
            {
                squareFootageValidation.Message = "\n\tERROR: That is not a valid entry for square footage!";
                if (e.Message == negSquareFootage)
                {
                    squareFootageValidation.Message = "\n\t" + negSquareFootage;
                }
                string logErrorMsg = "Error: square footage neg/zero" + "   User Entry: " + squareFootage;
                DataRepository.LogError(logErrorMsg);
                return squareFootageValidation;
            }
            squareFootageValidation.Success = true;
            squareFootageValidation.InputContent = squareFootageDecimal;

            return squareFootageValidation;
        }

        public ValidateDecimalResponse TaxValidation(string taxRate)
        {
            taxRate = taxRate.Trim('%');
            ValidateDecimalResponse taxRateValidation = new ValidateDecimalResponse();
            string negTaxRate = "\n\tERROR: Tax rate can not be a negative number!";
            decimal taxRateDecimal;
            try
            {
                taxRateDecimal = decimal.Parse(taxRate);
                if (taxRateDecimal <= 0)
                {
                    throw new SystemException(negTaxRate);
                }
            }
            catch (Exception e)
            {
                taxRateValidation.Message = "\n\tERROR: That is not a valid entry for tax rate!";
                if (e.Message == negTaxRate)
                {
                    taxRateValidation.Message = "\n\t" + negTaxRate;
                }
                string logErrorMsg = "Error: tax rate neg/zero" + "   User Entry: " + taxRate;
                DataRepository.LogError(logErrorMsg);
                return taxRateValidation;
            }
            taxRateValidation.Success = true;
            taxRateValidation.InputContent = taxRateDecimal * 0.01M;

            return taxRateValidation;
        }
    }
}