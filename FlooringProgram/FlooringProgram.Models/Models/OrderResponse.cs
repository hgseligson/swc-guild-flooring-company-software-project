﻿namespace FlooringProgram.Models.Models
{
    public class OrderResponse
    {
        public OrderResponse()
        {
            Order = new Order();
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public Order Order { get; set; }
    }
}