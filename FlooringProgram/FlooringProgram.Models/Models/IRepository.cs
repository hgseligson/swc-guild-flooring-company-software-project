﻿using System;
using System.Collections.Generic;

namespace FlooringProgram.Models.Models
{
    public interface IRepository
    {
        Dictionary<string, List<Order>> Orders { get; set; }

        List<Order> GetAll(string date);

        Order GetById(string date, int orderId);

        void UpdateOrder(string date, int orderId, Order updatedOrder);

        Order CreateOrder(string date, Order createdOrder);

        void DeleteOrder(string date, int orderId);

        void Save();

        void LogError(string error);

        List<TaxInfo> GetTaxInfo();

        List<ProductInfo> GetProductTypes();

        string ConvertDateTimeToString(DateTime dateTime);

        TaxInfo ReturnTaxInfo(string state);

        ProductInfo ReturnProductInfo(string product);
    }
}