﻿namespace FlooringProgram.Models.Models
{
    public class ValidateIntResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int InputContent { get; set; }
    }
}