﻿namespace FlooringProgram.Models.Models
{
    public class ValidateStringResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string InputContent { get; set; }
    }
}