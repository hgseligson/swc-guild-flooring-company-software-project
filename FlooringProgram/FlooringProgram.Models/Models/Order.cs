﻿using System;

namespace FlooringProgram.Models.Models
{
    public class Order
    {
        public int OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerName { get; set; }
        public decimal Area { get; set; }
        public ProductInfo ProdInfo { get; set; }
        public TaxInfo TaxInfo { get; set; }
        public decimal MaterialCost => Area * ProdInfo.CostPerSquareFoot;
        public decimal LaborCost => Area * ProdInfo.LaborPerSquareFoot;
        public decimal Tax => TaxInfo.TaxRate * (LaborCost + MaterialCost);
        public decimal Total => Tax + MaterialCost + LaborCost;
        public bool OrderCancelled { get; set; }
    }
}