﻿using System.Collections.Generic;

namespace FlooringProgram.Models.Models
{
    public class OrderListResponse
    {
        public OrderListResponse()
        {
            OrdersForDate = new List<Order>();
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public List<Order> OrdersForDate { get; set; }
    }
}