﻿namespace FlooringProgram.Models.Models
{
    public class ValidateDecimalResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public decimal InputContent { get; set; }
    }
}