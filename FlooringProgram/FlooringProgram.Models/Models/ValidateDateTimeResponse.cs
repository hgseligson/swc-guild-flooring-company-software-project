﻿using System;

namespace FlooringProgram.Models.Models
{
    public class ValidateDateTimeResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public DateTime InputContent { get; set; }
    }
}