﻿namespace FlooringProgram.Models.Models
{
    public class TaxInfo
    {
        public string State { get; set; }
        public string StateAbbreviation { get; set; }
        public decimal TaxRate { get; set; }
    }
}